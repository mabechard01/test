var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sassMiddleware = require('node-sass-middleware');

var Sequelize = require('sequelize');
var sequelizeObj = new Sequelize('test', 'root', 'toor', require('./config/default'));

//Dependency injection to reuse the connection to the BD
//Models are then injected in sequelizeObj once initialized
require("./dao/gameDAO")(sequelizeObj);
require("./dao/characterDAO")(sequelizeObj);
require("./dao/itemDAO")(sequelizeObj);
require("./dao/itemDescriptionDAO")(sequelizeObj);
require("./dao/characterItemsDAO")(sequelizeObj);

const characters = require('./routes/characterRouter')(sequelizeObj);
const games = require('./routes/gameRouter')(sequelizeObj);
const items = require('./routes/itemRouter')(sequelizeObj);
const itemDescriptions = require('./routes/itemDescriptionRouter')(sequelizeObj);
var index = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname,'public','images','favicon.ico')));
app.use('/', index);
app.use('/characters', characters);
app.use('/games', games);
app.use('/characters/:id/items', items);
app.use('/characters/:id/items/:id/', itemDescriptions);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.render('error', {
      error: req.app.get('env') === 'development' ? err : {},
      message: err.message
  });
});


module.exports = app;
