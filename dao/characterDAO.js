var validator = require('validator');

//Returns a DAO representing the table.
module.exports = function (sequelizeObj) {
    const Sequelize = sequelizeObj.Sequelize;
    sequelizeObj.models.Character = sequelizeObj.define('TES_USER', {
        id: {
            type: Sequelize.INTEGER(11),
            field: "USR_ID",
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(30),
            field: "USR_USERNAME",
            allowNull: false,
            validate:{
                notEmpty: true
            }
        },
        salt: {
            type: Sequelize.STRING(45),
            field: "USR_SALT"
        },
        hashed_password: {
            type: Sequelize.STRING,
            field: "USR_HASHEDPW"
        },
        image: {
            type: Sequelize.BLOB("long"),
            field: "USR_IMAGE",
            allowNull: false
        },
        extension: {
            type: Sequelize.STRING,
            field: "USR_EXTENSION",
            allowNull: false
        }
    }, {
        freezeTableName: true,
        timestamps: false
    });

    sequelizeObj.models.Game.hasMany(sequelizeObj.models.Character, {
        as : "charactersOfGame",
        foreignKey : {
            name : "game",
            field : "USR_DEPARTMENT",
            type : Sequelize.INTEGER(11)
        }
    });

    sequelizeObj.models.Character.belongsTo(sequelizeObj.models.Game, {
        as : "gameOfCharacter",
        foreignKey : {
            name : "game",
            field : "USR_DEPARTMENT",
            type : Sequelize.INTEGER(11)
        }
    });
}