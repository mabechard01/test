var validator = require('validator');

//Returns a DAO representing the table.
module.exports = function (sequelizeObj) {
    const Sequelize = sequelizeObj.Sequelize;
    sequelizeObj.models.CharacterItems = sequelizeObj.define('TES_CHARACTERITEMS', {
        quantity:{
            type: Sequelize.INTEGER,
            field: "CXI_QUANTITY",
            allowNull: false
        }
    }, {
        freezeTableName: true,
        timestamps: false
    });



    sequelizeObj.models.Character.belongsToMany(sequelizeObj.models.Item, {
        as : "itemsOfCharacter",
        foreignKey : {
            name : "character",
            field : "CXI_CHARACTER",
            type : Sequelize.INTEGER(11),
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        through: 'TES_CHARACTERITEMS'
    });

    sequelizeObj.models.Item.belongsToMany(sequelizeObj.models.Character, {
        as : "charactersOfItem",
        foreignKey : {
            name : "item",
            field : "CXI_ITEM",
            type : Sequelize.INTEGER(11),
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        through: 'TES_CHARACTERITEMS'
    });

}