//Returns a DAO representing the table.
module.exports = function (sequelizeObj) {
    const Sequelize = sequelizeObj.Sequelize;
    sequelizeObj.models.Game = sequelizeObj.define('TES_DEPARTMENT', {
        name: {
            type: Sequelize.STRING(30),
            field: "DEP_NAME",
            allowNull: false,
            validate:{
                notEmpty:true
            }
        },
        id: {
            type: Sequelize.INTEGER(11),
            field: "DEP_ID",
            primaryKey: true,
            autoIncrement: true
        },
        song: {
            type: Sequelize.BLOB("long"),
            field: "DEP_SONG"
        },
        song_extension: {
            type: Sequelize.STRING,
            field: "DEP_SONG_EXTENSION"
        },
        background: {
            type: Sequelize.BLOB("long"),
            field: "DEP_BACKGROUND",
            allowNull: false
        },
        bg_extension: {
            type: Sequelize.STRING,
            field: "DEP_BG_EXTENSION",
            allowNull: false
        }
    }, {
        freezeTableName: true,
        timestamps: false
    });
}