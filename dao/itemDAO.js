var validator = require('validator');

//Returns a DAO representing the table.
module.exports = function (sequelizeObj) {
    const Sequelize = sequelizeObj.Sequelize;
    sequelizeObj.models.Item = sequelizeObj.define('TES_ITEM', {
        id: {
            type: Sequelize.INTEGER(11),
            field: "ITE_ID",
            primaryKey: true,
            autoIncrement: true
        },
        image: {
            type: Sequelize.BLOB("long"),
            field: "ITE_IMAGE",
            allowNull: false
        },
        extension: {
            type: Sequelize.STRING,
            field: "ITE_EXTENSION"
        },
        name:{
          type: Sequelize.STRING,
          field: "ITE_NAME",
          allowNull: false,
            validate:{
                notEmpty: true
            }
        }
    }, {
        freezeTableName: true,
        timestamps: false
    });
}