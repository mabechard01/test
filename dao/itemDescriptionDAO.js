var validator = require('validator');

//Returns a DAO representing the table.
module.exports = function (sequelizeObj) {
    const Sequelize = sequelizeObj.Sequelize;
    sequelizeObj.models.ItemDescription = sequelizeObj.define('TES_ITEM_DESCRIPTION', {
        id: {
            type: Sequelize.INTEGER(11),
            field: "DES_ID",
            allowNull: false,
            primaryKey:true,
            autoIncrement:true
        },
        body: {
            type: Sequelize.STRING,
            field: "DES_BODY",
            allowNull: false
        }
    }, {
        freezeTableName: true,
        timestamps: false
    });

    sequelizeObj.models.ItemDescription.belongsTo(sequelizeObj.models.Item, {
        as : "itemOfDescription",
        foreignKey : {
            name : "item",
            field : "DES_ITEM",
            type : Sequelize.INTEGER(11),
        }
    });

    sequelizeObj.models.Item.hasOne(sequelizeObj.models.ItemDescription, {
        as : "descriptionOfItem",
        foreignKey : {
            name : "item",
            field : "DES_ITEM",
            type : Sequelize.INTEGER(11),
        }
    });
}