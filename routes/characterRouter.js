var express = require('express');
var sha1 = require('sha1');
var crypto = require('crypto-random-string');
var formidable = require('formidable');


module.exports = function (sequelizeObj) {
    let characterRouter = express.Router();

    /* GET users listing. */
    characterRouter.get('/', function (req, res, next) {
        sequelizeObj.models.Character.findAll({
            order:[
                ['name', 'ASC']
            ]
        }).then((characters)=>{
            res.render("list/characterList", {characters})
        }).catch((err)=>{
            next(err);
        })
    });

    characterRouter.use('/:id', function (req, res, next) {
        var id = req.params.id;
        if (isNaN(id)) {
            next();
        } else {
            sequelizeObj.models.Character.findById(id).then((character) => {
                character.getGameOfCharacter().then((game)=>{
                    req.game = game;
                    req.character = character;
                    next();
                })
            })
        }
    });

    characterRouter.get('/:id/view', function (req, res, next) {
        req.isOk = true;
        try {
            return res.render("read/character", {
                character: req.character,
                game : req.game
            });
        } catch (err) {
            console.log(err);
        }
    });

    characterRouter.get('/:id/characterImage', function (req, res, next) {
        res.set('Content-Type', req.character.extension);
        res.set('Content-Length', req.character.image.length);
        res.send(req.character.image);
    });


    characterRouter.get('/create', function (req, res, next) {
        sequelizeObj.models.Game.findAll(({
            order:[
                ['name', 'ASC']
            ]
        })).then((games)=>{
            res.render('create/characterForm', {games});
        })
    });

    characterRouter.post('/newCharacter', function (req, res, next) {

        formidable.IncomingForm().parse(req, function (err, fields, files) {
            req.fields = fields;
            req.files = files;
            if (err) {
                next(err);
            } else {
                next();
            }
        });

    }, function (req, res, next) {
        var salt = crypto(45);
        sequelizeObj.models.Character.create({
            name: req.fields.name,
            salt,
            hashed_password: sha1(req.fields.password + salt),
            image: require("fs").readFileSync(req.files.image.path),
            extension: req.files.image.type,
            game: req.fields.game
        }).then((character) => {
            res.redirect('/characters/' + character.id + '/view');
        }).catch(err => {
            console.log(err);
            res.redirect("back")
        });
    });

    return characterRouter;
}
