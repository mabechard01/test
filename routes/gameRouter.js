var express = require('express');
var formidable = require('formidable');


module.exports = function (sequelizeObj) {
    let gameRouter = express.Router();
    /* GET users listing. */
    gameRouter.get('/', function (req, res, next) {
        sequelizeObj.models.Game.findAll({
            order:[
                ['name', 'ASC']
            ]
        }).then((games)=>{
            res.render("list/gameList", {games})
        }).catch((err)=>{
            next(err);
        })
    });

    gameRouter.use('/:id', function (req, res, next) {
        var id = req.params.id;
        if (isNaN(id)) {
            next();
        } else {
            sequelizeObj.models.Game.findById(id).then((game) => {
                game.getCharactersOfGame({
                    order:[
                        ['name', 'ASC']
                    ]
                }).then((charactersOfGame)=>{
                    req.game = game;
                    req.charactersOfGame = charactersOfGame;
                    next();
                })
            })
        }
    });

    gameRouter.get('/:id/song', function (req, res, next) {
        res.set('Content-Type', req.game.song_extension);
        res.set('Content-Length', req.game.song.length);
        res.send(req.game.song);
    });

    gameRouter.get('/:id/background', function (req, res, next) {
        res.set('Content-Type', req.game.bg_extension);
        res.set('Content-Length', req.game.background.length);
        res.send(req.game.background);
    });

    gameRouter.get('/:id/view', function (req, res, next) {
        req.isOk = true;
        try{
            res.render("read/game", {game: req.game, charactersOfGame: req.charactersOfGame})
        } catch (err) {
            console.log(err);
        }
    });


    gameRouter.get('/create', function (req, res, next) {
        res.render('create/gameForm');
    });

    gameRouter.post('/newGame', function (req, res, next) {

        formidable.IncomingForm().parse(req, function (err, fields, files) {
            req.fields = fields;
            req.files = files;
            if (err) {
                next(err);
            } else {
                next();
            }
        });

    }, function (req, res, next) {
        sequelizeObj.models.Game.create({
            name: req.fields.name,
            song: require("fs").readFileSync(req.files.song.path),
            song_extension: req.files.song.type,
            background: require("fs").readFileSync(req.files.background.path),
            bg_extension: req.files.background.type
        }).then((game) => {
            res.redirect(`/games/${game.id}/view`);
        }).catch(err => {
            res.render("error", {error:err})
        });
    });
    return gameRouter;
}
