var express = require('express');
var formidable = require('formidable');

module.exports = function (sequelizeObj) {
    let itemRouter = express.Router();

    itemRouter.get('/', function (req, res, next) {
        req.character.getItemsOfCharacter({
            order:[
                ['name', 'ASC']
            ]
        }).then((items)=>{
            req.items = items;
        }).then(()=>{
            res.render("list/itemList", {items: req.items, character: req.character, game: req.game})
        }).catch((err)=>{
            next(err);
        })
    });

    itemRouter.use('/:id', function (req, res, next) {
        var id = req.params.id;
        if (isNaN(id)) {
            next();
        } else {
            sequelizeObj.models.Item.findById(id).then((item) => {
                item.getDescriptionOfItem().then((description)=>{
                    item.description = description.body;
                    req.item = item;
                    next();
                })
            })
        }
    });

    itemRouter.get('/:id/view', function (req, res, next) {
        req.isOk = true;
        try{
            res.render("read/item", {game: req.game, character:req.character, item: req.item})
        } catch (err) {
            console.log(err);
            next(err);
        }
    });


    itemRouter.get('/create', function (req, res, next) {
        sequelizeObj.models.CharacterItems.findAll({
            attributes:['item'],
            raw:true,
            where:{
                character: req.character.id
            }
        }).then((alreadyOwnedItems)=>{
            let ownedItemIds = [];
            alreadyOwnedItems.forEach((itemObj)=>{
                ownedItemIds.push(itemObj.item)
            })
            let where = (ownedItemIds.length!==0) ? `WHERE ITE_ID NOT IN ${JSON.stringify(ownedItemIds).replace('[', '(').replace(']', ')')}` : "";
            sequelizeObj.query(`SELECT ITE_NAME as 'name', ITE_ID as 'id'
            FROM TES_ITEM ${where}`,
                { type: sequelizeObj.QueryTypes.SELECT})
                .then((items)=>{
                res.render('create/itemForm', {
                    character: req.character,
                    game: req.game,
                    items
                });
            })
        })
    });

    itemRouter.post('/newItem', function (req, res, next) {
        formidable.IncomingForm().parse(req, function (err, fields, files) {
            req.fields = fields;
            req.files = files;
            if (err) {
                next(err);
            } else {
                next();
            }
        });

    }, function (req, res, next) {
        sequelizeObj.models.Item.create({
            name: req.fields.name,
            extension: req.files.image.type,
            image: require("fs").readFileSync(req.files.image.path)
        }).then((item) => {
            sequelizeObj.models.CharacterItems.create({
                character: req.character.id,
                item: item.id,
                quantity: 1
            }).then((item)=>{
                console.log(item)
                sequelizeObj.models.ItemDescription.create({
                    item: item.item,
                    body: req.fields.description
                }).then(()=>{
                    res.redirect(`/characters/${req.character.id}/items/`)
                })
            })
        }).catch(err => {
            res.render("error", {error:err})
        });
    });


    itemRouter.post('/addItem', function (req, res, next) {
        formidable.IncomingForm().parse(req, function (err, fields, files) {
            req.fields = fields;
            if (err) {
                next(err);
            } else {
                next();
            }
        });

    }, function (req, res, next) {
        sequelizeObj.models.CharacterItems.create({
            character: req.character.id,
            item: req.fields.item,
            quantity: 1
        }).then(()=>{
            res.redirect(`/characters/${req.character.id}/items/`);
        }).catch(err => {
            res.render("error", {error:err})
        });
    });

    itemRouter.get('/:id/updateForm', function (req, res, next) {
        res.render('update/item', {
            character: req.character,
            game: req.game,
            item: req.item
        });
    });

    itemRouter.post('/:id/update', function (req, res, next) {
        formidable.IncomingForm().parse(req, function (err, fields, files) {
            req.fields = fields;
            req.files = files;
            if (err) {
                next(err);
            } else {
                next();
            }
        });

    }, function (req, res, next) {
        req.item.update({
            name: req.fields.name,
            extension: req.files.image.type,
            image: require("fs").readFileSync(req.files.image.path)
        }).then(()=>{
            sequelizeObj.models.ItemDescription.findOne({
                where:{
                    item: req.item.id
                }
            }).then((itemDescription)=>{
                itemDescription.update({
                    body: req.fields.description
                }).then(()=>{
                    res.redirect(`/characters/${req.character.id}/items/`);
                }).catch(err => {
                    res.render("error", {error:err})
                });
            })
        })
    });


    itemRouter.get('/:id/image', function (req, res, next) {
        res.set('Content-Type', req.item.extension);
        res.set('Content-Length', req.item.image.length);
        res.send(req.item.image);
    });

    return itemRouter;
}
