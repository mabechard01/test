var express = require('express');
var formidable = require('formidable');

module.exports = function (sequelizeObj) {
    let itemDescriptionRouter = express.Router();

    itemDescriptionRouter.get('/', function (req, res, next) {
        req.Item.descriptionOfItem().then((description)=>{
            res.send({itemDescription : description.body});
        }).catch((err)=>{
            next(err);
        })
    });

    return itemDescriptionRouter;
}
